package com.rando.service.imp;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rando.dao.EtapeRepository;
import com.rando.dao.ItineraireRepository;
import com.rando.modele.Etape;
import com.rando.modele.Itineraire;
import com.rando.service.EtapeService;

@Service
@Transactional
public class EtapeServiceImp implements EtapeService{
	
	@Autowired
	EtapeRepository etapeRepository;
	
	@Autowired
	ItineraireRepository itineraireRepository;

	//récupérer toutes les étapes
	public List<Itineraire> getEtapes() {
		List<Etape> listeEtapes = new ArrayList<Etape>();
		List<Itineraire> listeItineraire = new ArrayList<Itineraire>();
		
		itineraireRepository.findAll().forEach(listeItineraire::add);
		etapeRepository.findAll().forEach(listeEtapes::add);
		
		return listeItineraire;
	}
	
	// récupérer une étape
	public Etape getEtape(int idEtape) {
		
		Optional<Etape> getEtape = etapeRepository.findById(idEtape);
		Etape etape = getEtape.get();
	
		return etape;
	}
	
	public void saveEtape(Etape etapeASauver) throws Exception {
		List<Etape> listeEtapes = etapeRepository.findAll();
		for(Etape e : listeEtapes) {
			if(e.getItineraireEtape().getIdItineraire() ==  etapeASauver.getItineraireEtape().getIdItineraire() && e.getNumeroEtape() == etapeASauver.getNumeroEtape()) {
				throw new Exception("Numéro d'étape existe déjà");
			}
		}
		etapeRepository.save(etapeASauver);
	}
	
	public void deleteEtape(String idEtape) {
		etapeRepository.deleteByEtapeId(Integer.parseInt(idEtape));
	}
}
