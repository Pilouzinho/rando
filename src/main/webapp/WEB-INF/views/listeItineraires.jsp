<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ma Randonnée</title>
<link rel="stylesheet" href="<c:url value='/styles/style.css'/>">
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>

	<h1>Admin</h1>
	<h1>Ma randonnée</h1>
	<div id= "menu">
		<div id="Accueil">
			<a href="<c:url value="/"/>">Accueil</a>
		</div>
		<div id="Itineraires">
			<a href="<c:url value="/listeItineraires"/>">Itinéraires</a> 
		</div>
		<div id="Etapes">
			<a href="<c:url value="/listeEtapes"/>">Étapes</a>  
		</div>
	</div>
	<h3>Itinéraires</h3>
	
	<c:if  test="${ not empty messageAjoutFormulaire }">
	    <div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
	</c:if>
	<c:if  test="${ not empty messageErreurFormulaire }">
	    <div class="message-erreur"><c:out value="${messageErreurFormulaire }"/></div>
	</c:if>
	
	<a href="<c:url value="/creerItineraire"/>"><button id="creerItineraire">Créer un itinéraire</button></a>
	<div id="tableauItineraire">
		
		<table>
			 <tr>
		       <th>Itinéraire</th>
		       <th>Nombre d'étapes</th>
		       <th>Niveau</th>
   			</tr>
   			<c:forEach items="${listeItineraires }" var="itineraire">
   				<tr>
   					<td><h2><a href="<c:out value="${pageContext.servletContext.contextPath}/itineraire/${itineraire.idItineraire }" />"><c:out value="${itineraire.nomItineraire}"/></a></h2>
   					<td><p><c:out value="${itineraire.listeEtapesItineraire.size()}"></c:out></td>
   					<td><p><c:out value="${itineraire.niveauItineraire.libelleNiveau }"></c:out></p></td>
   				</tr>
   			</c:forEach>
			</table>

	</div>
	</body>
</html>