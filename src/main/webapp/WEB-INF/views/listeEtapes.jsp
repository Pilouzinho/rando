<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ma Randonnée</title>
<link rel="stylesheet" href="<c:url value='/includes/styles/style.css'/>">
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>

	<h1>Admin</h1>
	<h1>Ma randonnée</h1>
	<div id= "menu">
		<div id="Accueil">
			<a href="<c:url value="/"/>">Accueil</a>
		</div>
		<div id="Itineraires">
			<a href="<c:url value="/listeItineraires"/>">Itinéraires</a> 
		</div>
		<div id="Etapes">
			<a href="<c:url value="/listeEtapes"/>">Étapes</a>  
		</div>
	</div>
	<h3>Étapes</h3>
	
	<c:if  test="${ not empty messageAjoutFormulaire }">
	    <div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
	</c:if>
	<c:if  test="${ not empty messageErreurFormulaire }">
	    <div class="message-erreur"><c:out value="${messageErreurFormulaire }"/></div>
	</c:if>
	
	<a href="<c:url value="/creerEtape"/>"><button id="creerEtape">Créer une étape</button></a>
	<div id="tableauEtapes">
		<table>
			 <tr>
       <th>Etape</th>
       <th>Description</th>
       <th>Itinéraire</th>
       <th>Ordre dans l'itinéraire</th>
			<tr>
			<c:forEach items="${listeEtapes }" var="itineraire">
				<c:forEach items="${itineraire.listeEtapesItineraire}"  var="etape"  >
					<tr>
	   					<td><h2><a href="<c:out value="${pageContext.servletContext.contextPath}/etape/${etape.idEtape }" />"><c:out value="${etape.nomEtape}"/></a></h2>
	   					<td><p><c:out value="${etape.descriptionEtape}"></c:out></td>
	   					<td><p><c:out value="${itineraire.nomItineraire }"></c:out></p></td>
	   					<td><p><c:out value="${etape.numeroEtape }"></c:out></p></td>
   					</tr>
				</c:forEach>
   			</c:forEach>
			</tr>
			</table>
	</div>
	</body>
</html>