<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include  page="/WEB-INF/views/header.jsp"></jsp:include>

	<h2>Bienvenue "Administrateur"</h2>
	<div id="choix">
		<a href="<c:url value="/creerEtape"/>"><button id="creerEtape">Créer une étape</button></a>
		<a href="<c:url value="/creerItineraire"/>"><button id="creerItineraire">Créer un itinéraire</button></a>
		<a href="<c:url value="/listeEtapes"/>"><button id="listeEtapes">Consulter la liste des étapes</button></a>
		<a href="<c:url value="/listeItineraires"/>"><button id="listeItineraires">Consulter la liste des itinéraires</button></a>
	</div>

<jsp:include  page="/WEB-INF/views/footer.jsp"></jsp:include>
