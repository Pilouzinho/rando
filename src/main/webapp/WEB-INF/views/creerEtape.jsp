<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<jsp:include  page="/WEB-INF/views/header.jsp"></jsp:include>

	<h3>Ajouter une étape</h3>
	
	<c:if  test="${ not empty messageAjoutFormulaire }">
	    <div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
	</c:if>
	<c:if  test="${ not empty messageErreurFormulaire }">
	    <div class="message-erreur"><c:out value="${messageErreurFormulaire }"/></div>
	</c:if>
	
	<div id="formulaire">
		<form:form method="POST"  action="${pageContext.servletContext.contextPath}/etape-ajout" modelAttribute="etapeAAjouter">
			<form:errors path="*" class="has-error" />
		    <table>
		       <tr>
		           <td><form:label path="nomEtape">Nom</form:label></td>
		           <td><form:input type="text" path="nomEtape"/></td>
		       </tr>
		       <tr>
		           <td><form:label path="descriptionEtape">Description</form:label></td>
		           <td><form:input type="text" path="descriptionEtape"/></td>
		       </tr>
		       <tr>
		           <td><form:label path="itineraireEtape">Itinéraire de l'étape</form:label></td>
		           <td>
			           	<form:select path="itineraireEtape.idItineraire">
			            	<form:option value="0" label="--- Sélectionnez ---" />
					 		<form:options items="${listeItineraire}" itemLabel="nomItineraire" itemValue="idItineraire" />
						</form:select>
					</td>
	           </tr>
	           <tr>
		           <td><form:label path="numeroEtape">Numéro de l'étape</form:label></td>
		           <td><form:input type="number" path="numeroEtape" /></td>
		       </tr>
		        <tr>
	               <td><input type="submit" value="Envoyer"/></td>
	           </tr>
		       </table>
		</form:form>
	</div>
	
<jsp:include  page="/WEB-INF/views/footer.jsp"></jsp:include>

	