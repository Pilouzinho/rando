<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ma Randonnée</title>
<link rel="stylesheet" href="<c:url value='/includes/styles/style.css'/>">
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>

	<h1>Admin</h1>
	<h1>Ma randonnée</h1>
	<div id= "menu">
		<div id="Accueil">
			<a href="<c:url value="/"/>">Accueil</a>
		</div>
		<div id="Itineraires">
			<a href="<c:url value="/listeItineraires"/>">Itinéraires</a> 
		</div>
		<div id="Etapes">
			<a href="<c:url value="/listeEtapes"/>">Étapes</a>  
		</div>
	</div>
	<h3>Modifier l'itinéraire: <c:out value="${itineraireAModifier.nomItineraire }" ></c:out></h3>
	
	<c:if  test="${ not empty messageAjoutFormulaire }">
	    <div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
	</c:if>
	<c:if  test="${ not empty messageErreurFormulaire }">
	    <div class="message-erreur"><c:out value="${messageErreurFormulaire }"/></div>
	</c:if>
	
	<div id="formulaire">
	<form:form method="POST"  action="${pageContext.servletContext.contextPath}/modifierItineraire" modelAttribute="itineraireAModifier">
			<form:hidden path="idItineraire" value="${itineraireAModifier.idItineraire }" />
			<form:errors path="*" class="has-error" />
		    <table>
		       <tr>
		           <td><form:label path="nomItineraire">Nom</form:label></td>
		           <td><form:input type="text" path="nomItineraire"/></td>
		       </tr>
		       <tr>
		           <td><form:label path="niveauItineraire">Niveau de l'itinéraire</form:label></td>
		           <td>
			           <form:select path="niveauItineraire.idNiveau">
			           		<form:option value="0" label="--- Sélectionnez ---" />
				           <c:forEach items="${listeNiveaux}" var="niveau">
						        <c:choose>
						            <c:when test="${niveau eq itineraireAModifier.niveauItineraire}">
						                <form:option value="${niveau.idNiveau}" selected="true">${niveau.libelleNiveau}</form:option>
						            </c:when>
						            <c:otherwise>
						                <form:option value="${niveau.idNiveau}">${niveau.libelleNiveau}</form:option>
						            </c:otherwise>
						        </c:choose> 
						    </c:forEach>
					  	</form:select>
					</td>
		           </tr>
		           <tr>
		               <td><input type="submit" value="Envoyer"/></td>
		           </tr>
		       </table>
		</form:form>
	</div>
	</body>
</html>
