<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ma Randonnée</title>
<link rel="stylesheet" href="<c:url value='/includes/styles/style.css'/>">
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>

	<h1>Admin</h1>
	<h1>Ma randonnée</h1>
	<div id= "menu">
		<div id="Accueil">
			<a href="<c:url value="/"/>">Accueil</a>
		</div>
		<div id="Itineraires">
			<a href="<c:url value="/listeItineraires"/>">Itinéraires</a> 
		</div>
		<div id="Etapes">
			<a href="<c:url value="/listeEtapes"/>">Étapes</a>  
		</div>
	</div>
	<h3>Nom de l'étape : <c:out value="${etape.nomEtape }"></c:out></h3>
	
	<!-- Message en cas de succès à la modification de l'étape -->
	<c:if  test="${ not empty messageAjoutFormulaire }">
	    <div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
	</c:if>
	
	<a href="<c:url value="/modifier-etape/${etape.idEtape }"/>"><button id="modifEtape">Modifier l'étape</button></a>
	<a href='#' onclick='javascript:window.open("http://myurl", "_blank", "scrollbars=1,resizable=1,height=300,width=450");' title='Supprimer'>Supprimer l'étape</a>
	<a href="<c:out value="${pageContext.servletContext.contextPath}/supprimer-etape/${etape.idEtape }" />">Supprimer</a>	
	<a href="${pageContext.servletContext.contextPath}/generation-rapport-etape.pdf?idEtape=${etape.idEtape }">Imprimer</a>
	<div id="detailItineraire">

		<b>Description</b>
		<p><c:out value="${etape.descriptionEtape }"></c:out></p>

		</div>
		</body>
</html>