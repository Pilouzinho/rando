<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Bienvenue</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
</head>
<body>
<div class="container-fluid top-container">
	<h1><c:out value="${nomPage }" /></h1>
</div>
	<div class="container">
  	</div>
  	
	<div class="container-fluid">
		<nav class="navbar navbar-expand-sm bg-light">
			<ul id="nav" class="navbar-nav flex-row-start-center">
				<li class="active nav-item"><a href="<c:url value="/accueil" />" class="nav-link">Accueil</a></li>
				<li class="active nav-item"><a href="<c:url value="listeItineraires" />" class="nav-link">Itinéraires</a></li>
				<li class="active nav-item"><a href="<c:url value="/listeEtapes" />" class="nav-link">Étapes</a></li>	
				<li><a href="<c:url value="/logout" />" >Déconnexion</a></li>
			</ul>
		</nav>
	</div>