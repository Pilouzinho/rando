<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ma Randonnée</title>
<link rel="stylesheet" href="<c:url value='/includes/styles/style.css'/>">
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>

	<h1>Admin</h1>
	<h1>Ma randonnée</h1>
	<div id= "menu">
		<div id="Accueil">
			<a href="<c:url value="/"/>">Accueil</a>
		</div>
		<div id="Itineraires">
			<a href="<c:url value="/listeItineraires"/>">Itinéraires</a> 
		</div>
		<div id="Etapes">
			<a href="<c:url value="/listeEtapes"/>">Étapes</a>  
		</div>
	</div>
	<h3>Modifier l'étape: nom de l'étape</h3>
	<div id="formulaire">
		<form:form method="POST"  action="${pageContext.servletContext.contextPath}/etape-modifier" modelAttribute="etapeAModifier">
			<form:hidden path="idEtape" value="${etapeAModifier.idEtape }" />
		
			<form:errors path="*" class="has-error" />
		    <table>
		       <tr>
		           <td><form:label path="nomEtape">Nom</form:label></td>
		           <td><form:input type="text" path="nomEtape"/></td>
		       </tr>
		       <tr>
		           <td><form:label path="descriptionEtape">Description</form:label></td>
		           <td><form:input type="text" path="descriptionEtape"/></td>
		       </tr>
		       <tr>
		           <td><form:label path="itineraireEtape">Itinéraire de l'étape</form:label></td>
		           <td>
			           	 <form:select path="itineraireEtape.idItineraire">
			           		<form:option value="0" label="--- Sélectionnez ---" />
				           <c:forEach items="${listeItineraire}" var="itineraire">
						        <c:choose>
						            <c:when test="${itineraire eq etapeAModifier.itineraireEtape}">
						                <form:option value="${itineraire.idItineraire}" selected="true">${itineraire.nomItineraire}</form:option>
						            </c:when>
						            <c:otherwise>
						                <form:option value="${itineraire.idItineraire}">${itineraire.nomItineraire}</form:option>
						            </c:otherwise>
						        </c:choose> 
						    </c:forEach>
					  	</form:select>
					</td>
	           </tr>
	           <tr>
		           <td><form:label path="numeroEtape">Numéro de l'étape</form:label></td>
		           <td><form:input type="number" path="numeroEtape" /></td>
		       </tr>
		        <tr>
	               <td><input type="submit" value="Envoyer"/></td>
	           </tr>
		       </table>
		</form:form>
	</div>
	</body>
</html>