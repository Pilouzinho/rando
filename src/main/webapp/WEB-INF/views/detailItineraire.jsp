<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Ma Randonnée</title>
<link rel="stylesheet" href="<c:url value='/includes/styles/style.css'/>">
<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
</head>
<body>

	<h1>Admin</h1>
	<h1>Ma randonnée</h1>
	<div id="menu">
		<div id="Accueil">
			<a href="<c:url value="/"/>">Accueil</a>
		</div>
		<div id="Itineraires">
			<a href="<c:url value="/listeItineraires"/>">Itinéraires</a> 
		</div>
		<div id="Etapes">
			<a href="<c:url value="/listeEtapes"/>">Étapes</a>  
		</div>
	</div>
	<h2><c:out value="${itineraire.nomItineraire }"></c:out></h2>
	
	<!-- Message en cas de succès à la modification de l'itinéraire -->
	<c:if  test="${ not empty messageAjoutFormulaire }">
	    <div class="message-validation"><c:out value="${messageAjoutFormulaire }"/></div>
	</c:if>
	
	<a href="<c:url value="/modifierItineraire/${itineraire.idItineraire }"/>"><button id="modifItineraire">Modifier l'itinéraire</button></a>
	<a href='#' onclick='javascript:window.open("http://myurl", "_blank", "scrollbars=1,resizable=1,height=300,width=450");' title='Supprimer'>Supprimer l'itinéraire</a>
	<a href="<c:out value="${pageContext.servletContext.contextPath}/supprimer-itineraire/${itineraire.idItineraire }" />">Supprimer</a>	
	<div id="detailItineraire">
		</bre>
		<h3>Niveau :</h3>
		<p><c:out value="${itineraire.niveauItineraire.libelleNiveau }"></c:out></p>
		<hl/>
		<h4>Liste des étapes : </h4>
		<table>
			 <tr>
		       <th>Nom de l'éape</th>
		       <th>Numéro de l'étape</th>
		       <th>Description de l'étape</th>
   			</tr>
   			<c:forEach items="${itineraire.listeEtapesItineraire }" var="etape">
   				<tr>
   					<td><h2><a href="<c:out value="${pageContext.servletContext.contextPath}/etape/${etape.idEtape }" />"><c:out value="${etape.nomEtape}"/></a></h2>
   					<td><p><c:out value="${etape.numeroEtape}"></c:out></td>
   					<td><p><c:out value="${etape.descriptionEtape }"></c:out></p></td>
   				</tr>
   			</c:forEach>
			</table>
	</div>
	</body>
</html>
		